#include <SoftwareSerial.h>

//#include <XBee.h>

#define NUMBYTES 80


int count = 0;
int scount = 0;
unsigned char buf[NUMBYTES];// = {1,215,1,216,1,217,1,218,1,219,1,220,1,221,1,222,1,223,1,224,1,225,1,226,1,227,1,228,1,229,1,230,1,231,1,232,1,233,1,234,1,235,1,236,1,237,1,238,1,239,1,240,1,241,1,242,1,243,1,244,1,245,1,246,1,247,1,248,1,249,1,250,1,251,1,252,1,253,1,254};

//Tx16Request tx = Tx16Request(0x1874, buf, sizeof(buf));

SoftwareSerial xbee(2,3);
//XBee xbee = XBee();

void setup() {
  pinMode(9, OUTPUT);
  xbee.begin(115200);
  
  //Serial.begin(115200);
}




void loop() {  
    //Serial.println();
    //delayMicroseconds(125);
    int level = analogRead(A2);
    buf[count] = level&0xFF;
    buf[count+1] = level >> 8;
    count += 2;
    if (count == NUMBYTES) {
      count = 0;
      scount++;
      //Swap the following two lines to alternate between local serial (test) mode and xbee mode.
      //Serial.write(buf, NUMBYTES);
      xbee.write(buf, NUMBYTES);
    }
    
    if (scount == 100) {
      scount = 0;
      digitalWrite(9, HIGH);
    }
    else if (scount == 1) {
      digitalWrite(9, LOW); 
    }

 
}
