#include <SoftwareSerial.h>

#include <XBee.h>

#define NUMBYTES 100

#define FASTADC 1

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif


int count = 0;
int scount = 0;
unsigned char buf[NUMBYTES];// = {128,1, 144,1, 159,1, 175,1, 189,1, 203,1, 215,1, 226,1, 236,1, 243,1, 249,1, 253,1, 255,1, 255,1, 253,1, 249,1, 243,1, 236,1, 226,1, 215,1, 203,1, 189,1, 175,1, 159,1, 144,1, 128,1, 111,1, 96,1, 80,1, 66,1, 52,1, 40,1, 29,1, 19,1, 12,1, 6,1, 2,1, 0,1, 0,1, 2,1, 6,1, 12,1, 19,1, 29,1, 40,1, 52,1, 66,1, 80,1, 96,1, 111,1};
unsigned char buf2[2] = {0,0};

XBee xbee1 = XBee();

//Tx16Request tx = Tx16Request(0x1874, buf, sizeof(buf));

SoftwareSerial xbee(2,3);

void setup() {
  
  
  #if FASTADC
 // set prescale to 16
 sbi(ADCSRA,ADPS2) ;
 cbi(ADCSRA,ADPS1) ;
 cbi(ADCSRA,ADPS0) ;
  #endif
  
  //pinMode(9, OUTPUT);
  xbee.begin(57600);
  //Serial.begin(115200);
  

}




void loop() {  
    //Serial.println();
    int level = analogRead(A2);
    //buf[count] = level & 0xFF;
    //buf[count+1] = level >> 8;
    //count += 2;
    buf2[0] = level & 0xFF;
    buf2[1] = level >> 8;
    xbee.write(buf2, 2);
    //Serial.write(buf2, 2);
    if (count == NUMBYTES) {
      count = 0;
      scount++;
      //xbee.write(buf, 100);
      //Swap the following two lines to alternate between local serial (test) mode and xbee mode.
      //Serial.write(buf, NUMBYTES);
      //xbee.write(buf, NUMBYTES);
      
    }
    
    /*
    if (scount == 100) {
      scount = 0;
      digitalWrite(9, HIGH);
    }
    else if (scount == 1) {
      digitalWrite(9, LOW); 
    }
    */
 
}
