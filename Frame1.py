#Boa:Frame:Frame1

import wx
import os
import glob
import thread
import serial

def create(parent):
    return Frame1(parent)

[wxID_FRAME1, wxID_FRAME1DIRPICKERCTRL1, wxID_FRAME1PANEL1, 
 wxID_FRAME1SERIALCOMBO, wxID_FRAME1STARTBUTTON, wxID_FRAME1STATICBOX1, 
 wxID_FRAME1STATICTEXT1, wxID_FRAME1STATICTEXT2, wxID_FRAME1STATICTEXT3, 
 wxID_FRAME1STATICTEXT4, wxID_FRAME1STATICTEXT5, wxID_FRAME1STATICTEXT6, 
 wxID_FRAME1THRESHOLD, wxID_FRAME1THRESHOLDTIME, wxID_FRAME1TRANSCRIBED, 
] = [wx.NewId() for _init_ctrls in range(15)]

class Frame1(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME1, name='', parent=prnt,
              pos=wx.Point(79, 199), size=wx.Size(415, 506),
              style=wx.DEFAULT_FRAME_STYLE,
              title=u'EE513 - Voice Transcriber XBEE Host')
        self.SetClientSize(wx.Size(415, 484))

        self.panel1 = wx.Panel(id=wxID_FRAME1PANEL1, name='panel1', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(415, 484),
              style=wx.TAB_TRAVERSAL)

        self.serialCombo = wx.ComboBox(choices=glob.glob('/dev/tty.*'), id=123,
              name=u'serialCombo', parent=self.panel1, pos=wx.Point(200, 14),
              size=wx.Size(200, 26), style=0, value=u'')
        self.serialCombo.SetLabel(u'')

        self.startButton = wx.ToggleButton(id=wxID_FRAME1STARTBUTTON,
              label=u'Start Transcription', name=u'startButton',
              parent=self.panel1, pos=wx.Point(152, 46), size=wx.Size(128, 20),
              style=0)
        self.startButton.SetValue(False)
        self.startButton.Bind(wx.EVT_TOGGLEBUTTON, self.OnStartButtonButton,
              id=wxID_FRAME1STARTBUTTON)

        self.staticText1 = wx.StaticText(id=wxID_FRAME1STATICTEXT1,
              label=u'Select XBEE Rx Port:', name='staticText1',
              parent=self.panel1, pos=wx.Point(56, 16), size=wx.Size(135, 17),
              style=0)

        self.transcribed = wx.TextCtrl(id=0, name='transcribed',
              parent=self.panel1, pos=wx.Point(16, 90), size=wx.Size(376, 100),
              style=0, value='')

        self.staticText2 = wx.StaticText(id=wxID_FRAME1STATICTEXT2,
              label=u'Transcription status:', name='staticText2',
              parent=self.panel1, pos=wx.Point(16, 72), size=wx.Size(111, 17),
              style=0)

        self.staticBox1 = wx.StaticBox(id=wxID_FRAME1STATICBOX1,
              label=u'Configuration', name='staticBox1', parent=self.panel1,
              pos=wx.Point(16, 200), size=wx.Size(376, 120), style=0)

        self.thresholdTime = wx.SpinCtrl(id=0, initial=2000, max=10000, min=500,
              name='thresholdTime', parent=self.panel1, pos=wx.Point(225, 224),
              size=wx.Size(72, 22), style=wx.SP_ARROW_KEYS)

        self.staticText3 = wx.StaticText(id=wxID_FRAME1STATICTEXT3,
              label=u'Sentence threshold (ms)', name='staticText3',
              parent=self.panel1, pos=wx.Point(64, 224), size=wx.Size(153, 24),
              style=0)

        self.staticText4 = wx.StaticText(id=wxID_FRAME1STATICTEXT4,
              label=u'Output file directory', name='staticText4',
              parent=self.panel1, pos=wx.Point(56, 278), size=wx.Size(153, 32),
              style=0)

        self.threshold = wx.SpinCtrl(id=0, initial=400, max=1024, min=0,
              name='threshold', parent=self.panel1, pos=wx.Point(225, 250),
              size=wx.Size(72, 22), style=wx.SP_ARROW_KEYS)

        self.dirPickerCtrl1 = wx.DirPickerCtrl(id=wxID_FRAME1DIRPICKERCTRL1,
              message='Select a folder', name='dirPickerCtrl1',
              parent=self.panel1, path=os.getcwd(), pos=wx.Point(184, 272),
              style=wx.DIRP_DEFAULT_STYLE)

        self.staticText5 = wx.StaticText(id=wxID_FRAME1STATICTEXT5,
              label=u'Threshold level (10 bit)', name='staticText5',
              parent=self.panel1, pos=wx.Point(64, 252), size=wx.Size(153, 32),
              style=0)

        self.statusLabel = wx.StaticText(id=0,
              label=u'Ready', name='statusLabel', parent=self.panel1,
              pos=wx.Point(148, 72), size=wx.Size(111, 17), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.thresholdTime.SetValue(2000)
        self.threshold.SetValue(400)
        self.kill = False
        self.statusLabel.SetForegroundColour(wx.GREEN)

        self.Show()

    def OnStartButtonButton(self, event):
        if "Start" in self.startButton.GetLabel():
            self.startButton.SetLabel("Stop Transcription")
            self.kill = False
            #thread.start_new_thread(self.sampleThread, ())
        else:
            self.startButton.SetLabel("Start Transcription")
            self.kill = True

    def sampleThread(self):
        s = serial.Serial(self.serialCombo.GetValue(), baudrate=115200)

        while not self.kill:
            self.statusLabel.SetLabel("Listening")
            self.statusLabel.SetForegroundColour(wx.YELLOW)
            data = []
            seenThreshold = False
            thresholdCount = 0
            while True:
                se = s.read(2)
                xnew = (ord(se[1])<<8) | ord(se[0])
                if xnew > 1024:
                    s.read(1)
                    continue
                data.append(xnew)
                pt = abs(xnew - MEAN_VALUE) #Center around 0

                #The idea is to slice up the input data around intervals 
                #based on thresholding.
                if not seenThreshold:
                    if pt > THRESHOLD:
                        if thresholdCount < MIN_COUNT:
                            thresholdCount += 1
                        else:
                            seenThreshold = True
                            thresholdCount = 0
                    else:
                        thresholdCount = 0
                else:
                    if pt < THRESHOLD:
                        if thresholdCount < MIN_COUNT:
                            thresholdCount += 1
                        else:
                            break
                    else:
                        thresholdCount = 0

            self.statusLabel.SetLabel("Processing")
            self.statusLabel.SetForegroundColour(wx.RED)
            data = array(data)            
            v = N.average(data)
            data -= av #Remove DC offset
            ydata = data * int(32768. / av)
            samples = SAMPLERATE * duration


            signal = N.resize(ydata, (samples,))

            ssignal = ''
            for i in range(len(signal)):
                if signal[i] > 32767:
                    signal[i] = 32767
                if signal[i] < -32767:
                    signal[i] = -32767
                ssignal += wave.struct.pack('h',signal[i]) # transform to binary

            f = SoundFile(ssignal)
            f.write()

            os.system("sh rateconv.sh")

            r = sr.Recognizer()
            with sr.WavFile("hi_rate_audio.wav") as source:     
                audio = r.record(source)                        
            try:
                tst = r.recognize(audio)   # recognize speech using Google Speech Recognition
            except LookupError:                                 # speech is unintelligible
                tst = ''

            tstr = time.strftime("%H:%M:%S") + "> " + tst + "\n"
            self.transcribed.SetValue(self.transcribed.GetValue() + tstr)

        s.close()

        



