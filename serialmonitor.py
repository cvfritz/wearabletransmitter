import serial
import thread
from numpy import *

bytes_per_message = 100

#s = serial.Serial('/dev/tty.usbmodem1431', baudrate=115200)
s = serial.Serial('/dev/tty.usbserial-DA01LE4W', baudrate=115200)
loc = thread.allocate_lock()

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import time

app = QtGui.QApplication([])

win = pg.GraphicsWindow(title="Microphone Level Monitor")
win.resize(1000,600)
win.setWindowTitle('Microphone Monitor')

pg.setConfigOptions(antialias=True)

p6 = win.addPlot(title="Updating plot")

curve = p6.plot(pen='y')
p6.setYRange(100, 600)

_data = []
_time = []

def update():
    global curve, _data, _time, p6
    loc.acquire()
    curve.setData(_time, _data)
    loc.release()
modCount = 0


def readSerial():
    global curve, _data, _time, p6, modCount
    ind = 0
    while True:
        try:
            loc.acquire()
            line = s.read(2)          
            newSample = ord(line[0]) | (ord(line[1]) << 8)
            if newSample > 1024:
                s.read(1)
                continue
            _data.append(newSample)
            _time.append(time.clock())

            if len(_data) > 10000:
                _data = _data[1:]
                _time = _time[1:]

            loc.release()
        except:
            s.close()

timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

thread.start_new_thread(readSerial, ())

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()


